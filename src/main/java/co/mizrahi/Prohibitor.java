package co.mizrahi;

import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.util.stream.Collectors;

@Mojo(name = "prohibitor", executionStrategy = "always",
        threadSafe = true, defaultPhase = LifecyclePhase.PROCESS_SOURCES)
public class Prohibitor extends AbstractMojo {

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    @Parameter(property = "artifact", readonly = true, required = true)
    private String artifactId;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        boolean match = project.getDependencies().stream()
                .map(Dependency::getArtifactId).collect(Collectors.toList()).contains(artifactId);
        if (match)
            throw new MojoFailureException("\n ** DEPENDENCY PROHIBITED: The project contains [" + artifactId +
                    "] dependency that is prohibited. **\n");
    }
}
