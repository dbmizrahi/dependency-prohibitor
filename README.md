# DEPENDENCY PROHIBITOR

This project is an example how to create custom maven plugin

## Description:

With this plugin "dependency-prohibitor" we can prohibit unnecessary dependency from parent pom.xml for prevent 
it's usage in any child module

## Usage:
    <build>
        ...
        <plugins>
            ...
            <plugin>
                <groupId>co.mizrahi</groupId>
                <artifactId>prohibitor</artifactId>
                <version>0.0.1</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prohibitor</goal>
                        </goals>
                        <configuration>
                            <artifactId>${artifactId}</artifactId>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            ...
        </plugins>
        ...
    </build>